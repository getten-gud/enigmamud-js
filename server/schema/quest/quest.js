var mongoose = require('mongoose');

var questSchema = new mongoose.Schema({
    title: {type:String, index: true, unique: true },
    followed_by: {type:mongoose.Schema.Types.ObjectId, ref:"Quest"},
    description: {type:String},

    segments: [{type:questSegment}]

})

var questSegment = new mongoose.Schema({
    description: {type:String},
    objectives: [{type:questObjective}],

})

var questObjective = new mongoose.Schema({
    description: {type:String},
    objectiveType: {type:String},
    objectiveData: {JSON}
})


exports.Model = mongoose.model("Quest", questSchema)