var mongoose = require('mongoose');

// Describes a 'race' with body parts and compatible character slots.
var characterTemplateSchema = new mongoose.Schema({
    name: {type:String, index: true, unique: true },
    body: [{type:bodyPartTemplateSchema}]

})

// Describes a part of the body and slot
var bodyPartTemplateSchema = new mongoose.Schema({
    name: {type:String},
    slot: {type:mongoose.Schema.Types.ObjectId, ref:"Character Slot"},
    order: {type: Number}
})


exports.Model = mongoose.model("Character Template", characterTemplateSchema)