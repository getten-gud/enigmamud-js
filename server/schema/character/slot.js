var mongoose = require('mongoose');

var slotSchema = new mongoose.Schema({
    slot_name: {type:String, index: true, unique: true },
    visible: {type:String, index:true},
})


exports.Model = mongoose.model("Item Slot", slotSchema)