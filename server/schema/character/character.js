var mongoose = require('mongoose');

var characterSchema = new mongoose.Schema({
    first_name: {type:String, index: true, unique: true },
    surname: {type:String, index:true},
    stats: {type:JSON}
})


exports.Model = mongoose.model("Character", characterSchema)