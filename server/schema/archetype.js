var mongoose = require('mongoose');

var archetypeSchema = new mongoose.Schema({
    title: {type:String, index: true, unique: true },
    parent: {type:mongoose.Schema.Types.ObjectId, ref:"Archetype"},
    description: {type:String},

})


exports.Model = mongoose.model("Archetype", archetypeSchema)