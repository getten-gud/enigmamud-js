var mongoose = require('mongoose');

var roomSchema = new mongoose.Schema({
    shortName: {type:String, index: true, unique: true },
    fullName: {type:String, index:true},
    description: {type: String},
    contents: {type: require('./item/itemPlacement').schema}
})


exports.Model = mongoose.model("Room", roomSchema)