var mongoose = require('mongoose');

//For items that exist in the real world.
exports.schema = new mongoose.Schema({
    //What item is this
    reference: {type:mongoose.Schema.Types.ObjectId, index: true, ref: "Item" },
    //Descriptive text (eg "under the table" to make the phrase "There is a yellow notebook under the table")
    location: {type:String, index:false},
    //How many things are there
    quantity: {type:Number},
    //Items which cannot simply be picked up See lockschema below for details. 
    locked: {type:lockSchema},
})

var lockSchema = new mongoose.Schema({
    //Does the lock block visibility of the item?
    blocking: {type:Boolean},
    //Phrase or phrases that this lock are identified by
    keyword: [{type:String}],
    //List of items that can open the lock
    key: [{type:mongoose.Schema.Types.ObjectId, ref: "Item"}],
    //List of archetypal objects that can open the lock
    archetype: [{type:mongoose.Schema.Types.ObjectId, ref: "Archetype"}],
    //List of people that can open the lock
    characters: [{type:mongoose.Schema.Types.ObjectId, ref:"Character"}],
})

g