var mongoose = require('mongoose');

var itemSchema = new mongoose.Schema({
    name: {type:String, index: true, unique: true },
    description: {type:String},
    slot: {type:mongoose.Schema.Types.ObjectId, ref: "Item Slot"}
})


exports.Model = mongoose.model("Item", itemSchema)